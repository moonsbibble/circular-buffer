#ifndef CBUFFER_H
#define CBUFFER_H

#include <ostream>
#include <iostream>
#include <cassert>

#include <iterator> 
#include <cstddef> 


/**
  @brief Array circolare di dati templati

  La classe rappresenta un array circolare cbuffer.
  La dimensione dell'array può essere scelta in fase di 
  costruzione e i dati verranno sovrascritti una volta
  terminato lo spazio.
*/

template <typename T>
class cbuffer {

public:
  typedef unsigned int size_type;
  typedef int index_type;
  typedef T value_type;


  /**
    @brief Costruttore di default

    Costruttore di default per istanziare un cbuffer vuoto.
  */
  cbuffer() : _size(0), _filledSize(0), _buffer(nullptr), _head(0), _tail(-1) {
  }

  /**
    @brief Costruttore secondario

    Costruttore secondario per istanziare un cbuffer 
    con una dimensione prefissata.

    @param size dimensione del cbuffer da creare
  */
  explicit cbuffer(size_type size) : _size(0), _filledSize(0), _buffer(nullptr), _head(0), _tail(-1) {
    _buffer = new value_type[size + 1];
    _size = size;
  }

  /**
    @brief Costruttore secondario 2

    Costruttore secondario per istanziare un cbuffer 
    con una dimensione prefissata e inizializzare le 
    celle dell'array con un valore passato.

    @param size dimensione del cbuffer da creare
    @param value valore di inizializzazione delle celle dell'array

    @throw std::bad_alloc possibile eccezione di allocazione
  */
  cbuffer(size_type size, const value_type &value) : _size(0), _filledSize(0), _buffer(nullptr), _head(0), _tail(-1) {
    _buffer = new value_type[size + 1];

    try {
	for (size_type i = 0; i < size; ++i)
		_buffer[i] = value;
	_size = size;
	_tail = size - 1;
	_filledSize = size;
    }
    catch(...) {
      clear();
      throw;
    }
  }

  /**
    @brief Copy constructor

    Costruttore di copia tra due un cbuffer.

    @param other cbuffer da copiare

    @throw std::bad_alloc possibile eccezione di allocazione
  */
  cbuffer(const cbuffer &other) : _size(0), _filledSize(0), _buffer(nullptr), _head(0), _tail(-1) {

    _buffer = new value_type[other._size + 1];
    try {
	    for (size_type i = 0; i < other._size; ++i)
	    	_buffer[i] = other._buffer[i];
	    
	    _head = other._head;
	    _tail = other._tail;
	    _size = other._size;
	    _filledSize = other._filledSize;
	}
    catch(...) {
		clear();
	  throw;  
    }
  }

  /**
    @brief Operatore di assegnamento

    Operatore di assegnamento per copiare il contenuto di 
    un cbuffer other nel cbuffer this.

    @param other cbuffer da copiare
    @return reference a cbuffer
  */
  cbuffer<T>& operator=(const cbuffer<T> &other) {

    if (this != &other) {
      cbuffer tmp(other);
      tmp.swap(*this);
    }

    return *this;
  }

  /**
    @brief Swap tra cbuffer

    Funzione per scambiare il contenuto di due cbuffer.

    @param other il cbuffer da scambiare con this
  */
  void swap(cbuffer &other) {
    std::swap(this->_size, other._size);
    std::swap(this->_filledSize, other._filledSize);
    std::swap(this->_buffer, other._buffer);
    std::swap(this->_head, other._head);
    std::swap(this->_tail, other._tail);
  }

  /**
    @brief Distruttore di un cbuffer

    Distruttore per rimuovere dallo heap 
    l'array circolare allocato precedentemente.
  */
  ~cbuffer(void) {
    clear();
  }

  /**
    Funzione che pulisce il contenuto di un cbuffer.

    @post _head == 0
    @post _tail == -1
    @post _size == 0
    @post _filledSize == 0
    @post _buffer = nullptr;
  */
  void clear() {
    delete[] _buffer;

    _size = 0;
    _filledSize = 0;
    _buffer = nullptr;
    _head = 0;
    _tail = -1;
  }

  /**
    @brief Operatore [] getter/setter

    Funzione che permette di leggere e/o scrivere la cella
    i-esima dell'array circolare compreso tra testa e coda.

    @param index della cella da cui leggere/scrivere

    @return reference alla cella index-esima 

    @throw possibile eccezione

    @pre _filledSize == 0
  */
  value_type& operator[](index_type index) {
  	if(_filledSize == 0) {
  		throw;
  	} else if ((_tail > _head) && !(index <= _tail && index >= _head)) {
  		throw;
  	} else if ((_tail < _head) && !((index <= _tail && index >= 0) || (index >= _head && index < _size))) {
    	throw;
    } else if (_tail == _head && (index != _head)) {
   		throw;
    }
    
    return *(_buffer + index);
  }

  /**
    @brief Operatore [] getter

    Funzione che permette di leggere la cella i-esima 
    dell'array circolare.

    @param index della cella da cui leggere/scrivere

    @return reference alla cella index-esima
  */
  const value_type& operator[](index_type index) const {
    
  	return *(_buffer + index);
  }

  /**
    @brief Dimensione totale

    Ritorna la dimensione totale dell'array 
    circolare.

    @return dimensione del cbuffer
  */
  size_type size() const {
    return _size;
  }

  /**
    @brief Dimensione occupata

    Ritorna la dimensione attualmente occupata 
    dell'array circolare.

    @return dimensione occupata
  */
  size_type filledSize() const {
    return _filledSize;
  }

  /**
    @brief Testa cbuffer

    Ritorna l'indice d'inizio di un cbuffer.

    @return head cbuffer
  */
  index_type getHead() const {
    return _head;
  }

  /**
    @brief Tail cbuffer

    Ritorna l'indice di fine di un cbuffer.

    @return tail cbuffer
  */
  index_type getTail() const {
    return _tail;
  }

  /**
    @brief Inserimento in coda

    Inserisce un nuovo elemento in coda rispetto
    a quelli presenti nel cbuffer, se il cbuffer
    è pieno sovrascrive il dato più vecchio.

    @throw possibile eccezione
  */
  void addTail(const value_type &value) {
  	if(_buffer == nullptr) {
  		clear();
  		throw;
  	}
    
    if(_tail == (_size - 1)) {
    	_tail = 0;
    } else {
    	_tail++;
    }

    if(_head == _tail && _filledSize > 1) {
    	if(_head == (_size - 1)) {
    		_head = 0;
    	} else {
    		_head++;
    	}
    }

    _buffer[_tail] = value;

    if(_filledSize < _size || _size == 0)
    	_filledSize++;
  }

  /**
    @brief Rimozione in testa

    Rimuove l'elemento situato in testa del
    cbuffer.

    @throw possibile eccezione
  */
  void removeHead() {
  	if(_filledSize == 0) {
  		clear();
  		throw;
  	}

	if(_filledSize == 1) {
		_head = 0;
		_tail = -1;
	} else if(_head == (_size - 1)) {
    	_head = 0;
    } else {
    	_head++;
    }
    _filledSize--;
  }

  /**
  @brief Operatore di stream

  Ridefinizione dell'operatore di stream per scrivere un
  cbuffer su uno stream di output, viene stampato il contenuto
  logico circolare e fisicamente disposto in memoria.

  @param os stream di output (operando di sinistra)
  @param cb cbuffer da scrivere (operando di destra)

  @return reference allo stream di output
  */
  friend std::ostream &operator<<(std::ostream &os, const cbuffer &cb) {

  	if(cb._filledSize > 0) {
  		int max = cb._head + cb._filledSize;

	    for (size_type i = cb._head; i < max; ++i) {
	      if(i == cb._size) {
	      	i = 0;
	      	max -= cb._size;
	      }
	      os << cb._buffer[i] << " ";
	    }
	}

    return os;
  }

  /**
    Costruttore per generare un cbuffer con dimensione size
    riempito con una sequenza di dati identificata da una coppia 
    di iteratori generici passati per argomento. 

    @param b iteratore di inizio sequenza
    @param e iteratore di fine sequenza

    @throw std::bad_alloc possibile eccezione di allocazione
  */
  template <typename Iter>
  cbuffer(size_type size, Iter b, Iter e) : _size(0), _filledSize(0), _buffer(nullptr), _head(0), _tail(-1) {
    _buffer = new value_type[size + 1];

    try {
      for(; b != e; ++b) {
        addTail(static_cast<T>(*b));
	  }
    
      _size = size;
    }
    catch(...) {
      clear();
      throw;
    }
  }

  class const_iterator;

	class iterator {
		//	
	public:
		typedef std::random_access_iterator_tag iterator_category;
		typedef T                        value_type;
		typedef ptrdiff_t                difference_type;
		typedef T*                       pointer;
		typedef T&                       reference;

	
		iterator() : _p(nullptr), _start(nullptr), _head(0), _tail(-1), _size(0), _filledSize(0) {
		}
		
		iterator(const iterator &other) : _p(other._p), _start(other._start), _head(other._head), _tail(other._tail), _size(other._size), _filledSize(other._filledSize) {	
		}

		iterator& operator=(const iterator &other) {
			_p = other._p;
			_start = other._start;
			_head = other._head;
			_tail = other._tail;
			_size = other._size;
			_filledSize = other._filledSize;

			return *this;
		}

		~iterator() {
		}

		// Ritorna il dato riferito dall'iteratore (dereferenziamento)
		reference operator*() const {
			return *_p;
		}

		// Ritorna il puntatore al dato riferito dall'iteratore
		pointer operator->() const {
			return _p;
		}

		// Operatore di accesso random
		reference operator[](index_type index) {
			if(_filledSize == 0) {
		  		throw;
  			}
			iterator tmp(*this);

		    return *(tmp + index);
		}
		
		// Operatore di iterazione post-incremento
		iterator operator++(int) {
			iterator tmp(*this);

			if(_p == (_start + _tail)) {
				_p = _start + _size;
			} else if(_p == (_start + _size)) {
				_p = _start + _head;
			} else if(_p == _start + (_size - 1)) {
				_p = _start;
			} else {
				_p++;
			}
			return tmp;
		}

		// Operatore di iterazione pre-incremento
		iterator &operator++() {
			if(_p == (_start + _tail)) {
				_p = _start + _size;
			} else if(_p == (_start + _size)) {
				_p = _start + _head;
			} else if(_p == _start + (_size - 1)) {
				_p = _start;
			} else {
				_p++;
			}
			return *this;
		}

		// Operatore di iterazione post-decremento
		iterator operator--(int) {
			iterator tmp(*this);

			if(_p == (_start + _head)) {
				_p = _start + _size;
			} else if(_p == (_start + _size)) {
				_p = _start + _tail;
			} else if(_p == _start) {
				_p = _start + (_size - 1);
			} else {
				_p--;
			}
			return tmp;
		}

		// Operatore di iterazione pre-decremento
		iterator &operator--() {
			if(_p == (_start + _head)) {
				_p = _start + _size;
			} else if(_p == (_start + _size)) {
				_p = _start + _tail;
			} else if(_p == _start) {
				_p = _start + (_size - 1);
			} else {
				_p--;
			}
			return *this;
		}

		// Spostamentio in avanti della posizione
		iterator operator+(int offset) {
			iterator result(*this);
			
			if(offset >= 0) {
				for(; offset > 0; ++result) {
					offset -= 1;
				}
			} else {
				offset *= -1;

				for(; offset > 0; --result) {
					offset -= 1;
				}
			}
			return result;
		}

		// Spostamentio all'indietro della posizione
		iterator operator-(int offset) {
			iterator result(*this);
			
			if(offset < 0) {
				offset *= -1;

				for(; offset > 0; ++result) {
					offset -= 1;
				}
			} else {
				for(; offset > 0; --result) {
					offset -= 1;
				}
			}
			return result;
		}
		
		// Spostamentio in avanti della posizione
		iterator& operator+=(int offset) {
			if(offset >= 0) {
				for(; offset > 0; ++(*this)) {
					offset -= 1;
				}
			} else {
				offset *= -1;

				for(; offset > 0; --(*this)) {
					offset -= 1;
				}
			}
			return *this;
		}

		// Spostamentio all'indietro della posizione
		iterator& operator-=(int offset) {
			if(offset < 0) {
				offset *= -1;

				for(; offset > 0; ++(*this)) {
					offset -= 1;
				}
			} else {
				for(; offset > 0; --(*this)) {
					offset -= 1;
				}
			}
			return *this;
		}

		// Numero di elementi tra due iteratori
		difference_type operator-(const iterator &other) {
			int count = 0;

			if(_head == other._head && _tail == other._tail) {
				if((*this) > other) {
					iterator result(other);

					for(; result != (*this); ++result) {
						count++;
					}
					return ((_start + count) - _start);
				} else if((*this) < other) {
					iterator result(*this);

					for(; result != other; ++result) {
						count++;
					}
					return (_start - (_start + count));
				} else {
					return (_p - other._p);
				}
			} else {
				return (_p - other._p);
			}
		}
	
		// Uguaglianza
		bool operator==(const iterator &other) const {
			return (_p == other._p);
		}

		// Diversita'
		bool operator!=(const iterator &other) const {
			return (_p != other._p);
		}

		// Confronto
		bool operator>(const iterator &other) const {
			if(_head == other._head && _tail == other._tail) {
				if(_tail < _head) {
					if(_p != (_start + _size) || other._p != (_start + _size)) {
						if(!((_p - _start) >= _head && (other._p - _start) >= _head) &&
							!((_p - _start) <= _tail && (other._p - _start) <= _tail))
							return (_p < other._p);
					}
				}
			}

			return (_p > other._p);
		}
		

		bool operator>=(const iterator &other) const {
			if(_head == other._head && _tail == other._tail) {
				if(_tail < _head) {
					if(_p != (_start + _size) || other._p != (_start + _size)) {
						if(!((_p - _start) >= _head && (other._p - _start) >= _head) &&
							!((_p - _start) <= _tail && (other._p - _start) <= _tail))
							return (_p <= other._p);
					}
				}
			}

			return (_p >= other._p);
		}

		// Confronto
		bool operator<(const iterator &other) const {
			if(_head == other._head && _tail == other._tail) {
				if(_tail < _head) {
					if(_p != (_start + _size) || other._p != (_start + _size)) {
						if(!((_p - _start) >= _head && (other._p - _start) >= _head) &&
							!((_p - _start) <= _tail && (other._p - _start) <= _tail))
							return (_p > other._p);
					}
				}
			}

			return (_p < other._p);
		}
		
		
		// Confronto
		bool operator<=(const iterator &other) const {
			if(_head == other._head && _tail == other._tail) {
				if(_tail < _head) {
					if(_p != (_start + _size) || other._p != (_start + _size)) {
						if(!((_p - _start) >= _head && (other._p - _start) >= _head) &&
							!((_p - _start) <= _tail && (other._p - _start) <= _tail))
							return (_p >= other._p);
					}
				}
			}

			return (_p <= other._p);
		}
		
		
		// Solo se serve anche const_iterator aggiungere le seguenti definizioni
		
		friend class const_iterator;

		// Uguaglianza
		bool operator==(const const_iterator &other) const {
			return (_p == other._p);
		}

		// Diversita'
		bool operator!=(const const_iterator &other) const {
			return (_p != other._p);
		}

		// Confronto
		bool operator>(const const_iterator &other) const {
			if(_head == other._head && _tail == other._tail) {
				if(_tail < _head) {
					if(_p != (_start + _size) || other._p != (_start + _size)) {
						if(!((_p - _start) >= _head && (other._p - _start) >= _head) &&
							!((_p - _start) <= _tail && (other._p - _start) <= _tail))
							return (_p < other._p);
					}
				}
			}

			return (_p > other._p);
		}
		

		bool operator>=(const const_iterator &other) const {
			if(_head == other._head && _tail == other._tail) {
				if(_tail < _head) {
					if(_p != (_start + _size) || other._p != (_start + _size)) {
						if(!((_p - _start) >= _head && (other._p - _start) >= _head) &&
							!((_p - _start) <= _tail && (other._p - _start) <= _tail))
							return (_p <= other._p);
					}
				}
			}

			return (_p >= other._p);
		}

		// Confronto
		bool operator<(const const_iterator &other) const {
			if(_head == other._head && _tail == other._tail) {
				if(_tail < _head) {
					if(_p != (_start + _size) || other._p != (_start + _size)) {
						if(!((_p - _start) >= _head && (other._p - _start) >= _head) &&
							!((_p - _start) <= _tail && (other._p - _start) <= _tail))
							return (_p > other._p);
					}
				}
			}

			return (_p < other._p);
		}
		
		
		// Confronto
		bool operator<=(const const_iterator &other) const {
			if(_head == other._head && _tail == other._tail) {
				if(_tail < _head) {
					if(_p != (_start + _size) || other._p != (_start + _size)) {
						if(!((_p - _start) >= _head && (other._p - _start) >= _head) &&
							!((_p - _start) <= _tail && (other._p - _start) <= _tail))
							return (_p >= other._p);
					}
				}
			}

			return (_p <= other._p);
		}

		// Solo se serve anche const_iterator aggiungere le precedenti definizioni
	
	private:
		//Dati membro

		// La classe container deve essere messa friend dell'iteratore per poter
		// usare il costruttore di inizializzazione.
		friend class cbuffer;

		// Costruttore privato di inizializzazione usato dalla classe container
		// tipicamente nei metodi begin e end
		iterator(T *p, T *start, index_type head, index_type tail, size_type size, size_type filledSize) { 
			_p = p;
			_start = start;
			_head = head;
			_tail = tail;
			_size = size;
			_filledSize = filledSize; 
		}
		
		T *_p;
		T *_start;
		index_type _head;
		index_type _tail;
		size_type _size;
		size_type _filledSize;
		
	}; // classe iterator
	
	// Ritorna l'iteratore all'inizio della sequenza dati
	iterator begin() {
		index_type testa = _head;

		if(_filledSize == 0) {
			testa = -1;
		}

		return iterator(&_buffer[testa], _buffer, testa, _tail, _size, _filledSize);
	}
	
	// Ritorna l'iteratore alla fine della sequenza dati
	iterator end() {
		return iterator(&_buffer[_size], _buffer, _head, _tail, _size, _filledSize);
	}
	
	
	
	class const_iterator {
		//	
	public:
		typedef std::random_access_iterator_tag iterator_category;
		typedef T                        value_type;
		typedef ptrdiff_t                difference_type;
		typedef const T*                 pointer;
		typedef const T&                 reference;

	
		const_iterator() : _p(nullptr), _start(nullptr), _head(0), _tail(-1), _size(0), _filledSize(0) {
		}
		
		const_iterator(const const_iterator &other) : _p(other._p), _start(other._start), _head(other._head), _tail(other._tail), _size(other._size), _filledSize(other._filledSize) {
		}

		const_iterator& operator=(const const_iterator &other) {
			_p = other._p;
			_start = other._start;
			_head = other._head;
			_tail = other._tail;
			_size = other._size;
			_filledSize = other._filledSize;

			return *this;
		}

		~const_iterator() {
		}

		// Ritorna il dato riferito dall'iteratore (dereferenziamento)
		reference operator*() const {
			return *_p;
		}

		// Ritorna il puntatore al dato riferito dall'iteratore
		pointer operator->() const {
			return _p;
		}

		// Operatore di accesso random
		reference operator[](index_type index) {
			if(_filledSize == 0) {
		  		throw;
  			}
			const_iterator tmp(*this);

		    return *(tmp + index);
		}
		
		// Operatore di iterazione post-incremento
		const_iterator operator++(int) {
			const_iterator tmp(*this);

			if(_p == (_start + _tail)) {
				_p = _start + _size;
			} else if(_p == (_start + _size)) {
				_p = _start + _head;
			} else if(_p == _start + (_size - 1)) {
				_p = _start;
			} else {
				_p++;
			}
			return tmp;
		}

		// Operatore di iterazione pre-incremento
		const_iterator &operator++() {
			if(_p == (_start + _tail)) {
				_p = _start + _size;
			} else if(_p == (_start + _size)) {
				_p = _start + _head;
			} else if(_p == _start + (_size - 1)) {
				_p = _start;
			} else {
				_p++;
			}
			return *this;
		}

		// Operatore di iterazione post-decremento
		const_iterator operator--(int) {
			const_iterator tmp(*this);

			if(_p == (_start + _head)) {
				_p = _start + _size;
			} else if(_p == (_start + _size)) {
				_p = _start + _tail;
			} else if(_p == _start) {
				_p = _start + (_size - 1);
			} else {
				_p--;
			}
			return tmp;
		}

		// Operatore di iterazione pre-decremento
		const_iterator &operator--() {
			if(_p == (_start + _head)) {
				_p = _start + _size;
			} else if(_p == (_start + _size)) {
				_p = _start + _tail;
			} else if(_p == _start) {
				_p = _start + (_size - 1);
			} else {
				_p--;
			}
			return *this;
		}

		// Spostamentio in avanti della posizione
		const_iterator operator+(int offset) {
			const_iterator result(*this);
			
			if(offset >= 0) {
				for(; offset > 0; ++result) {
					offset -= 1;
				}
			} else {
				offset *= -1;

				for(; offset > 0; --result) {
					offset -= 1;
				}
			}
			return result;
			
		}

		// Spostamentio all'indietro della posizione
		const_iterator operator-(int offset) {
			const_iterator result(*this);

			if(offset < 0) {
				offset *= -1;

				for(; offset > 0; ++result) {
					offset -= 1;
				}
			} else {
				for(; offset > 0; --result) {
					offset -= 1;
				}
			}
			return result;
			
		}
		
		// Spostamentio in avanti della posizione
		const_iterator& operator+=(int offset) {
			if(offset >= 0) {
				for(; offset > 0; ++(*this)) {
					offset -= 1;
				}
			} else {
				offset *= -1;

				for(; offset > 0; --(*this)) {
					offset -= 1;
				}
			}
			return *this;
		}

		// Spostamentio all'indietro della posizione
		const_iterator& operator-=(int offset) {
			if(offset < 0) {
				offset *= -1;

				for(; offset > 0; ++(*this)) {
					offset -= 1;
				}
			} else {
				for(; offset > 0; --(*this)) {
					offset -= 1;
				}
			}
			return *this;
		}

		// Numero di elementi tra due iteratori
		difference_type operator-(const const_iterator &other) {
			int count = 0;

			if(_head == other._head && _tail == other._tail) {
				if((*this) > other) {
					const_iterator result(other);

					for(; result != (*this); ++result) {
						count++;
					}
					return ((_start + count) - _start);
				} else if((*this) < other) {
					const_iterator result(*this);

					for(; result != other; ++result) {
						count++;
					}
					return (_start - (_start + count));
				} else {
					return (_p - other._p);
				}
			} else {
				return (_p - other._p);
			}
		}
	
		// Uguaglianza
		bool operator==(const const_iterator &other) const {
			return (_p == other._p);
		}

		// Diversita'
		bool operator!=(const const_iterator &other) const {
			return (_p != other._p);
		}

		// Confronto
		bool operator>(const const_iterator &other) const {
			if(_head == other._head && _tail == other._tail) {
				if(_tail < _head) {
					if(_p != (_start + _size) || other._p != (_start + _size)) {
						if(!((_p - _start) >= _head && (other._p - _start) >= _head) &&
							!((_p - _start) <= _tail && (other._p - _start) <= _tail))
							return (_p < other._p);
					}
				}
			}

			return (_p > other._p);
		}
		

		bool operator>=(const const_iterator &other) const {
			if(_head == other._head && _tail == other._tail) {
				if(_tail < _head) {
					if(_p != (_start + _size) || other._p != (_start + _size)) {
						if(!((_p - _start) >= _head && (other._p - _start) >= _head) &&
							!((_p - _start) <= _tail && (other._p - _start) <= _tail))
							return (_p <= other._p);
					}
				}
			}

			return (_p >= other._p);
		}

		// Confronto
		bool operator<(const const_iterator &other) const {
			if(_head == other._head && _tail == other._tail) {
				if(_tail < _head) {
					if(_p != (_start + _size) || other._p != (_start + _size)) {
						if(!((_p - _start) >= _head && (other._p - _start) >= _head) &&
							!((_p - _start) <= _tail && (other._p - _start) <= _tail))
							return (_p > other._p);
					}
				}
			}

			return (_p < other._p);
		}
		
		
		// Confronto
		bool operator<=(const const_iterator &other) const {
			if(_head == other._head && _tail == other._tail) {
				if(_tail < _head) {
					if(_p != (_start + _size) || other._p != (_start + _size)) {
						if(!((_p - _start) >= _head && (other._p - _start) >= _head) &&
							!((_p - _start) <= _tail && (other._p - _start) <= _tail))
							return (_p >= other._p);
					}
				}
			}

			return (_p <= other._p);
		}
		
		
		// Solo se serve anche iterator aggiungere le seguenti definizioni
		
		friend class iterator;

		// Uguaglianza
		bool operator==(const iterator &other) const {
			return (_p == other._p);
		}

		// Diversita'
		bool operator!=(const iterator &other) const {
			return (_p != other._p);
		}

		// Confronto
		bool operator>(const iterator &other) const {
			if(_head == other._head && _tail == other._tail) {
				if(_tail < _head) {
					if(_p != (_start + _size) || other._p != (_start + _size)) {
						if(!((_p - _start) >= _head && (other._p - _start) >= _head) &&
							!((_p - _start) <= _tail && (other._p - _start) <= _tail))
							return (_p < other._p);
					}
				}
			}

			return (_p > other._p);
		}
		

		bool operator>=(const iterator &other) const {
			if(_head == other._head && _tail == other._tail) {
				if(_tail < _head) {
					if(_p != (_start + _size) || other._p != (_start + _size)) {
						if(!((_p - _start) >= _head && (other._p - _start) >= _head) &&
							!((_p - _start) <= _tail && (other._p - _start) <= _tail))
							return (_p <= other._p);
					}
				}
			}

			return (_p >= other._p);
		}

		// Confronto
		bool operator<(const iterator &other) const {
			if(_head == other._head && _tail == other._tail) {
				if(_tail < _head) {
					if(_p != (_start + _size) || other._p != (_start + _size)) {
						if(!((_p - _start) >= _head && (other._p - _start) >= _head) &&
							!((_p - _start) <= _tail && (other._p - _start) <= _tail))
							return (_p > other._p);
					}
				}
			}

			return (_p < other._p);
		}
		
		
		// Confronto
		bool operator<=(const iterator &other) const {
			if(_head == other._head && _tail == other._tail) {
				if(_tail < _head) {
					if(_p != (_start + _size) || other._p != (_start + _size)) {
						if(!((_p - _start) >= _head && (other._p - _start) >= _head) &&
							!((_p - _start) <= _tail && (other._p - _start) <= _tail))
							return (_p >= other._p);
					}
				}
			}

			return (_p <= other._p);
		}

		// Costruttore di conversione iterator -> const_iterator
		const_iterator(const iterator &other) : _p(other._p), _start(other._start), _head(other._head), _tail(other._tail), _size(other._size), _filledSize(other._filledSize) {
		}

		// Assegnamento di un iterator ad un const_iterator
		const_iterator &operator=(const iterator &other) {
			_p = other._p;
			_start = other._start;
			_head = other._head;
			_tail = other._tail;
			_size = other._size;
			_filledSize = other._filledSize;

			return *this;
		}

		// Solo se serve anche iterator aggiungere le precedenti definizioni
	
	private:
		//Dati membro

		// La classe container deve essere messa friend dell'iteratore per poter
		// usare il costruttore di inizializzazione.
		friend class cbuffer; // !!! Da cambiare il nome!

		// Costruttore privato di inizializzazione usato dalla classe container
		// tipicamente nei metodi begin e end
		const_iterator(const T *p, T *start, index_type head, index_type tail, size_type size, size_type filledSize) { 
			_p = p;
			_start = start;
			_head = head;
			_tail = tail;
			_size = size;
			_filledSize = filledSize; 
		}
		
		const T *_p;
		T *_start;
		index_type _head;
		index_type _tail;
		size_type _size;
		size_type _filledSize;
		
	}; // classe const_iterator
	
	// Ritorna l'iteratore all'inizio della sequenza dati
	const_iterator begin() const {
		index_type testa = _head;

		if(_filledSize == 0) {
			testa = -1;
		}

		return const_iterator(&_buffer[testa], _buffer, testa, _tail, _size, _filledSize);
	}
	
	// Ritorna l'iteratore alla fine della sequenza dati
	const_iterator end() const {
		return const_iterator(&_buffer[_size], _buffer, _head, _tail, _size, _filledSize);
	}

private:
  size_type _size; // Dimensione cbuffer
  size_type _filledSize; // Dimensione occupata cbuffer
  value_type *_buffer; // Buffer circolare
  index_type _head; // Indice testa (elemento più vecchio)
  index_type _tail; // Indice coda (elemento più recente)
};


#endif
