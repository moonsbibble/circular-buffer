/**
@file main.cpp 
@brief test d'uso della classe cbuffer
**/

#include <iostream>
#include <fstream>
#include "cbuffer.h"
#include <cassert>

using namespace std;

/**
 Test dei metodi fondamentali 

 @brief Test dei metodi fondamentali 

 */

void test_fondamentali_int() {

  std::cout << "\n*** TEST METODI FONDAMENTALI ***" << std::endl;

  std::cout << "test ctor default" << std::endl;
  cbuffer<int> db1;
  assert(db1.size() == 0); // verifichiamo lo stato dell'oggetto

  std::cout << "test ctor secondario 1" << std::endl;
  cbuffer<int> db2(100);
  assert(db2.size() == 100);

  std::cout << "test ctor secondario 2" << std::endl;
  cbuffer<int> db3(50, -1);
  assert(db3.size() == 50);
  for(unsigned int i=0; i < 50; i++)
    assert(db3[i] == -1);

  std::cout << "test copy constructor" << std::endl;
  cbuffer<int> db4(db3);
  assert(db4.size() == db3.size());
  for(unsigned int i=0 ;i < db4.size(); i++)
    assert(db3[i] == db4[i]);

  std::cout << "test operatore assegnamento =" << std::endl;
  db1 = db3;
  assert(db1.size() == db3.size());
  for(unsigned int i =0 ;i < db1.size(); i++)
    assert(db3[i] == db1[i]);
}

/**
 Test dei metodi secondari

 @brief Test dei metodi secondari 

 */

void test_metodi_int() {

  std::cout << "\n*** TEST METODI cbuffer<int> ***" << std::endl;

  cbuffer<int> db(5,0);

  std::cout << "test scrittura op[]" << std::endl;
  db[3] = 5;
  assert(db[3]==5);

  // Se compilazione in modalita' debug da' errore 
  //db[100] = 5;

  std::cout << "test lettura op[]" << std::endl;
  int a = db[3];
  assert(a==5);

  // Se compilazione in modalita' debug da' errore 
  //int tmp = db[8];

  std::cout << "test stampa con operator<<" << std::endl;
  std::cout << db << std::endl;

  std::cout << "test funzione swap" << std::endl;
  cbuffer<int> db1(5, 0);
  cbuffer<int> db2(10, 3);
  db1.swap(db2);

  assert(db1.size() == 10);
  assert(db2.size() == 5);

  for(unsigned int i=0 ;i < db1.size(); i++)
    assert(db1[i] == 3);

  for(unsigned int i=0 ;i < db2.size(); i++)
    assert(db2[i] == 0);

  std::cout << "test filledSize" << std::endl;
  assert(db1.filledSize() == db1.size());
  assert(db2.filledSize() == db2.size());

}

/**
 Test aggiunta elementi in coda da un cbuffer di interi

 @brief Test add  

 */

void test_add_int() {

	std::cout << "\n*** TEST METODO addTail() ***" << std::endl;
	std::cout << "inserimento dopo array completamente inizializzato" << std::endl;

    cbuffer<int> c(5, -2);
    assert(c.getHead() == 0);
    assert(c.getTail() == 4);

    c.addTail(12);
    c.addTail(44);
    c.addTail(99);

    assert(c.getHead() == 3);
    assert(c.getTail() == 2);

    assert(c[0] == 12);
    assert(c[1] == 44);
    assert(c[2] == 99);
    assert(c[3] == -2);
    assert(c[4] == -2);
    //assert(c[5] == 0); non posso leggere elementi privati

    assert(c.filledSize() == 5);


    std::cout << "inserimento con array vuoto" << std::endl;

    cbuffer<int> c2(5);
    assert(c2.getHead() == 0);
    assert(c2.getTail() == -1);

    c2.addTail(3);
    c2.addTail(4);
    c2.addTail(5);

    assert(c2.getHead() == 0);
    assert(c2.getTail() == 2);

    assert(c2[0] == 3);
    assert(c2[1] == 4);
    assert(c2[2] == 5);
    // assert(c2[3] == 0); non posso leggere elementi privati
    // assert(c2[4] == 0); non posso leggere elementi privati
    // assert(c2[5] == 0); non posso leggere elementi privati

    assert(c2.filledSize() == 3);

    std::cout << "inserimento con array vuoto sovrascrittura" << std::endl;

    cbuffer<int> c3(5);
    assert(c3.getHead() == 0);
    assert(c3.getTail() == -1);
    assert(c3.filledSize() == 0);

    c3.addTail(3);
    c3.addTail(4);
    c3.addTail(5);
    c3.addTail(12);
    c3.addTail(55);
    c3.addTail(10);
    c3.addTail(99);
    c3.addTail(22);
    c3.addTail(33);

    assert(c3.getHead() == 4);
    assert(c3.getTail() == 3);

    assert(c3[0] == 10);
    assert(c3[1] == 99);
    assert(c3[2] == 22);
    assert(c3[3] == 33);
    assert(c3[4] == 55);
    //assert(c3[5] == 0); non posso leggere elementi privati

    assert(c3.filledSize() == 5);

}

/**
 Test rimozione elementi in testa da un cbuffer di interi

 @brief Test remove  

 */

void test_remove_int() {

	std::cout << "\n*** TEST METODO removeHead() ***" << std::endl;
	std::cout << "rimozione dopo array completamente inizializzato" << std::endl;

	cbuffer<int> c2(5, 23);
    assert(c2.getHead() == 0);
    assert(c2.getTail() == 4);

    c2.removeHead();
    c2.removeHead();

    assert(c2.getHead() == 2);
    assert(c2.getTail() == 4);

    //assert(c2[0] == 0); non posso leggere elementi privatim
    //assert(c2[1] == 0); non posso leggere elementi privati
    assert(c2[2] == 23);
    assert(c2[3] == 23); 
    assert(c2[4] == 23);
    //assert(c3[5] == 0); non posso leggere elementi privati

    std::cout << "rimozione completa" << std::endl;

    cbuffer<int> c3(5, 2);
    assert(c3.getHead() == 0);
    assert(c3.getTail() == 4);

    c3.removeHead();
    c3.removeHead();
    c3.removeHead();
    c3.removeHead();

    assert(c3.getHead() == 4);
    assert(c3.getTail() == 4);

    //assert(c3[0] == 0); non posso leggere elementi privati
    //assert(c3[1] == 0); non posso leggere elementi privati
    //assert(c3[2] == 0); non posso leggere elementi privati
    //assert(c3[3] == 0); non posso leggere elementi privati
    assert(c3[4] == 2);
    //assert(c3[5] == 0); non posso leggere elementi privati

    cout << "stampa con iteratori: 2" << endl;
    cbuffer<int>::iterator b;
    for(b = c3.begin(); b != c3.end(); ++b) {
    	cout << *b << endl;
    }

    cout << "stampa con ostream: 2" << endl;
    cout << c3 << endl;

    c3.removeHead();
    assert(c3.getHead() == 0);
    assert(c3.getTail() == -1);

    cout << c3 << endl;

    c3.addTail(12);
    c3.addTail(24);

    assert(c3.getHead() == 0);
    assert(c3.getTail() == 1);

    std::cout << "traslazione indici testa-coda" << std::endl;

    cbuffer<int> c4(5, 2);

    c4.removeHead();
    c4.removeHead();
    c4.removeHead();
    c4.removeHead();
    c4.addTail(44);
    c4.addTail(22);
    c4.addTail(33);
    c4.addTail(99);

    assert(c4.getHead() == 4);
    assert(c4.getTail() == 3);

    assert(c4[0] == 44);
    assert(c4[1] == 22);
    assert(c4[2] == 33);
    assert(c4[3] == 99);
    assert(c4[4] == 2);
    //assert(c4[5] == 0);

    cout << "stampa con iteratori: 2 44 22 33 99" << endl;
    for(b = c4.begin(); b != c4.end(); ++b) {
    	cout << *b << endl;
    }

    c4.addTail(12);

    assert(c4.getHead() == 0);
    assert(c4.getTail() == 4);

    assert(c4[0] == 44);
    assert(c4[1] == 22);
    assert(c4[2] == 33);
    assert(c4[3] == 99);
    assert(c4[4] == 12);
    //assert(c4[5] == 0);

    cout << "stampa con iteratori: 44 22 33 99 12" << endl;
    for(b = c4.begin(); b != c4.end(); ++b) {
    	cout << *b << endl;
    }

}

/**
 Test cbuffer costante (operazioni di lettura)

 @brief Test cbuffer costante  

 */

void test_const_int(const cbuffer<int> &db) {

  // Metodi in scrittura (errore)
  //db[1] = 10; // errore
  //db.addTail(12); // errore

  // Metodi in lettura
  int tmp1 = db[3];
  int size = db.size();
  int filledSize = db.filledSize();
  int head = db.getHead();
  int tail = db.getTail();

  std::cout << "test stampa con operator<<" << std::endl;
  std::cout << db << std::endl;

  cbuffer<int> db2;
  //db2.swap(db); // errore
}

/**
 Test iteratori costanti di interi (costruttori e operatori)

 @brief Test iteratori costanti di interi  

 */

void test_const_iterator_int() {

  std::cout << "\n*** TEST ITERATORI COSTANTI ***" << std::endl;
  cout << "test base" << endl;
  cbuffer<int> d(10);

  for (unsigned int j = 0; j < 10; ++j)
    d.addTail(j);

  cbuffer<int>::const_iterator it; 
  cbuffer<int>::const_iterator ite;

  ite = d.end(); 

  cout << "stampa: 0 1 2 3 4 5 6 7 8 9" << endl;
  for (it = d.begin(); it != ite; ++it)
    std::cout << *it << std::endl;

  it = d.begin();


  cbuffer<int>::const_iterator cit;

  cit = d.begin();

  cout << "metodi fondamentali" << endl;

  cbuffer<int> c4(5, 2);

  cbuffer<int>::const_iterator b, e; 
  b = c4.begin();
  e = b;

  cbuffer<int>::const_iterator f(e);
  assert(b == e && e == f);

  cout << "operatore []" << endl;

  c4.removeHead();
  c4.removeHead();
  c4.removeHead();
  c4.removeHead();
  c4.addTail(44);
  c4.addTail(22);
  c4.addTail(33);
  c4.addTail(99);

  assert(c4.getHead() == 4);
  assert(c4.getTail() == 3);

  assert(c4[0] == 44);
  assert(c4[1] == 22);
  assert(c4[2] == 33);
  assert(c4[3] == 99);
  assert(c4[4] == 2);

  b = c4.begin();
  assert(b[0] == 2); // b punta ad head
  assert(b[1] == 44);
  assert(b[2] == 22);
  assert(b[3] == 33);
  assert(b[4] == 99);

  b++;

  assert(b[4] == 0); // b punta ad head + 1
  assert(b[0] == 44);
  assert(b[1] == 22);
  assert(b[2] == 33);
  assert(b[3] == 99);

  b+=2;

  assert(b[0] == 33); // b punta ad head + 3
  assert(b[1] == 99);
  assert(b[2] == 0);
  assert(b[3] == 2);
  assert(b[4] == 44);

  cout << "operatori ++, --, +=, -=, +, -" << endl;

  b = c4.begin();
  b += 5;
  b--;
  assert(*b == c4[c4.getTail()]);

  b += -3;
  assert(*b == 44);
  assert(*b == c4[0]);

  b -= -3;
  assert(*b == c4[c4.getTail()]);

  b++;
  b--;
  b += -2;
  b -= -3;
  b++;
  assert(*b == c4[c4.getHead()]);

  b = c4.begin();
  e = b + 4; // punta alla coda
  b += 5;    // punta al terminatore

  assert(*e == c4[c4.getTail()]);

  // ciclo al contrario const_iteratori
  for(; e != b; --e) {
    cout << *e << endl;
  }

  e = c4.begin() - 8;
  assert(*e == c4[c4.getTail()]);

  cout << "operatori <, >, >=, <=, ==, !=, diff_p" << endl;

  cbuffer<int> c5(5, 2);
  c4.removeHead();
  c4.removeHead();
  c4.removeHead();
  c4.removeHead();
  c4.addTail(44);
  c4.addTail(22);
  c4.addTail(33);
  c4.addTail(99);

  b = c5.begin();
  e = b + 5;
  assert(e > b);
  assert(e >= b);
  e = b + 4;
  assert(e > b);
  e = b + 1;
  assert(e > b);
  assert(b <= e);
  assert(b < e);
  e = c5.begin();
  assert(b == e);

  cbuffer<int> c6(5, 2);
  cbuffer<int>::const_iterator r1;

  c6.addTail(8);
  c6.addTail(93);
  c6.removeHead();

  r1 = c6.begin();

  assert(c6.getHead() == 3);
  assert(c6.getTail() == 1);
  assert(r1 < (r1 + 1));
  assert(r1 < (r1 + 2));
  assert(r1 < (r1 + 4));
  assert((r1 + 2) >= r1);
  assert((r1 + 2) <= (r1 + 3));
  assert((r1 + 2) <= (r1 + 2));
  assert((r1 + 3) >= (r1 + 2));

  assert((r1 + 3) - r1 == 3);
  assert((r1 + 2) - r1 == 2);
  assert((r1 + 1) - r1 == 1);
  assert(r1 - (r1 + 4) == -4);
  assert(r1 - (r1 + 3) == -3);
  assert(r1 - (r1 + 2) == -2);
  assert(r1 - (r1 + 1) == -1);

  //costruttore const_iterator(iterator)
  cbuffer<int>::iterator b1;
  b1 = c6.begin();
  cout << "iterator -> const_iterator" << endl;
  cbuffer<int>::const_iterator b3(b1);

  b3 += 1;
  assert(b3 > b1);

}

/**
 Test iteratori di interi (costruttori e operatori)

 @brief Test iteratori di interi  

 */

void test_iterator_int() {

  std::cout << "\n*** TEST ITERATORI ***" << std::endl;
  cout << "test base" << endl;
  cbuffer<int> d(10);

  for (unsigned int j = 0; j < 10; ++j)
    d.addTail(j);

  cbuffer<int>::iterator it; // iteratore di inizio lettura/scrittura
  cbuffer<int>::iterator ite;// iteratore di fine lettura/scrittura

  ite = d.end(); // chiediamo l'iteratore di fine

  cout << "stampa: 0 1 2 3 4 5 6 7 8 9" << endl;
  for (it = d.begin(); it != ite; ++it)
    std::cout << *it << std::endl;

  it = d.begin();
  *it = 100;

  assert(d[0] == 100);


  cbuffer<int>::iterator cit; // iteratore in lettura

  cit = d.begin();

  assert(*cit == 100);

  cout << "metodi fondamentali iteratori" << endl;

  cbuffer<int> c4(5, 2);

  cbuffer<int>::iterator b, e; 
  b = c4.begin();
  e = b;

  cbuffer<int>::iterator f(e);
  assert(b == e && e == f);

  cout << "operatore []" << endl;

  c4.removeHead();
  c4.removeHead();
  c4.removeHead();
  c4.removeHead();
  c4.addTail(44);
  c4.addTail(22);
  c4.addTail(33);
  c4.addTail(99);

  assert(c4.getHead() == 4);
  assert(c4.getTail() == 3);

  assert(c4[0] == 44);
  assert(c4[1] == 22);
  assert(c4[2] == 33);
  assert(c4[3] == 99);
  assert(c4[4] == 2);

  b = c4.begin();
  assert(b[0] == 2); // b punta ad head
  assert(b[1] == 44);
  assert(b[2] == 22);
  assert(b[3] == 33);
  assert(b[4] == 99);

  b++;

  assert(b[4] == 0); // b punta ad head + 1
  assert(b[0] == 44);
  assert(b[1] == 22);
  assert(b[2] == 33);
  assert(b[3] == 99);

  b+=2;

  assert(b[0] == 33); // b punta ad head + 3
  assert(b[1] == 99);
  assert(b[2] == 0);
  assert(b[3] == 2);
  assert(b[4] == 44);

  cout << "operatori ++, --, +=, -=, +, -" << endl;

  b = c4.begin();
  b += 5;
  b--;
  assert(*b == c4[c4.getTail()]);

  b += -3;
  assert(*b == 44);
  assert(*b == c4[0]);

  b -= -3;
  assert(*b == c4[c4.getTail()]);

  b++;
  b--;
  b += -2;
  b -= -3;
  b++;
  assert(*b == c4[c4.getHead()]);

  b = c4.begin();
  e = b + 4; // punta alla coda
  b += 5;    // punta al terminatore

  assert(*e == c4[c4.getTail()]);

  // ciclo al contrario iteratori
  for(; e != b; --e) {
    cout << *e << endl;
  }

  e = c4.begin() - 8;
  assert(*e == c4[c4.getTail()]);

  cout << "operatori <, >, >=, <=, ==, !=, diff_p" << endl;

  cbuffer<int> c5(5, 2);
  c4.removeHead();
  c4.removeHead();
  c4.removeHead();
  c4.removeHead();
  c4.addTail(44);
  c4.addTail(22);
  c4.addTail(33);
  c4.addTail(99);

  //test confronti tra iteratori
  b = c5.begin();
  e = b + 5;
  assert(e > b);
  assert(e >= b);
  e = b + 4;
  assert(e > b);
  e = b + 1;
  assert(e > b);
  assert(b <= e);
  assert(b < e);
  e = c5.begin();
  assert(b == e);

  cbuffer<int> c6(5, 2);
  cbuffer<int>::iterator r1;

  c6.addTail(8);
  c6.addTail(93);
  c6.removeHead();

  r1 = c6.begin();

  //test differenza tra iteratori
  assert(c6.getHead() == 3);
  assert(c6.getTail() == 1);
  assert(r1 < (r1 + 1));
  assert(r1 < (r1 + 2));
  assert(r1 < (r1 + 4));
  assert((r1 + 2) >= r1);
  assert((r1 + 2) <= (r1 + 3));
  assert((r1 + 2) <= (r1 + 2));
  assert((r1 + 3) >= (r1 + 2));

  assert((r1 + 3) - r1 == 3);
  assert((r1 + 2) - r1 == 2);
  assert((r1 + 1) - r1 == 1);
  assert(r1 - (r1 + 4) == -4);
  assert(r1 - (r1 + 3) == -3);
  assert(r1 - (r1 + 2) == -2);
  assert(r1 - (r1 + 1) == -1);

}

/**
 Test costruttore cbuffer con coppia di iteratori

 @brief Test costruttore con iteratori  

 */

void test_ctor_iterators() {

  std::cout << "\n*** TEST CTOR ITERATORS ***" << std::endl;
  
  cbuffer<double> c(10, 2);
  c.addTail(12);
  c.addTail(14);

  //cstor cbuffer con coppia di iteratori
  cbuffer<double>::iterator b;
  b = c.begin();
  cbuffer<double>::iterator e(c.end());
  cout << c << endl;
  cbuffer<int> c2(10, b, e);
  cout << c2 << endl;

  //cstor cbuffer con coppia di coppia iteratori costanti
  cbuffer<double>::const_iterator b1;
  b1 = c.begin();
  cbuffer<double>::const_iterator e1(c.end());
  cbuffer<int> c3(10, b1, e1);
  cout << c3 << endl;
}

/**
 Struct che identifica un punto a 2 coordinate.

 @brief Struct point(x, y).
 */
struct point {
  int _x;
  int _y;

  point() : _x(0), _y(0) {
  }

  point(int x, int y) : _x(x), _y(y) {
  }

  bool operator==(const point &p) const {
    return (this->_x == p._x && this->_y == p._y); 
  }
}; 

ostream &operator<<(std::ostream &os, const point &p) {
   cout << "(" << p._x << "," << p._y << ")";
   return os;  
}

/**
 Test cbuffer di char.

 @brief Test cbuffer char.
 */ 

void test_char_type() {

  std::cout << "\n*** TEST cbuffer<char> ***" << std::endl;
  cbuffer<char> c(5, 'r');

  //add / remove
  c.addTail('c');
  c.addTail('c');
  c.addTail('i');
  c.addTail('a');
  c.addTail('o');
  c.removeHead();
  //operatore <<
  cout << "stampa: ciao" << endl;
  cout << c << endl;

  //metodi cbuffer
  assert(c.filledSize() == 4);
  assert(c.size() == 5);
  cbuffer<char> c1(5);
  c1.addTail('c');
  c1.addTail('c');
  assert(c1.getHead() == 0);
  assert(c1.getTail() == 1);

  //copy constructor
  cbuffer<char> c2(c);
  //operatore <<
  cout << "stampa: ciao" << endl;
  cout << c2 << endl;

  //operatore []
  assert(c1[0] == c[1]);
  assert(c1[1] == c[1]);

  //assegnamento cbuffer
  cbuffer<char> c3(10, 'f');
  c3 = c;
  assert(c3.size() == 5);
  assert(c3[1] != 'f');

}

/**
 Test cbuffer di point.

 @brief Test cbuffer point.
 */ 

void test_point_type() {

  std::cout << "\n*** TEST cbuffer<struct point> ***" << std::endl;
  cbuffer<struct point> c(5);
  
  //add / remove
  c.addTail(point(2, 3));
  c.addTail(point(5, 7));
  c.addTail(point(8, 8));
  c.addTail(point(0, 0));
  c.addTail(point(2, 2));
  c.removeHead();
  //operatore <<
  cout << "stampa punti" << endl;
  cout << c << endl;

  //metodi cbuffer
  assert(c.filledSize() == 4);
  assert(c.size() == 5);
  cbuffer<struct point> c1(5);
  c1.addTail(point(0, 0));
  c1.addTail(point(1, 1));
  assert(c1.getHead() == 0);
  assert(c1.getTail() == 1);

  //copy constructor
  cbuffer<struct point> c2(c);
  //operatore <<
  cout << "stampa punti" << endl;
  cout << c2 << endl;

  //operatore []
  assert(c1[0] == c[3]);

  //assegnamento cbuffer
  cbuffer<struct point> c3(10);
  c3 = c;
  assert(c3.size() == 5);
  cout << "c3[1] = " << c3[1] << endl;
  assert(c3[1] == point(5, 7));
}



int main(int argc, char *argv[]) {

	test_fondamentali_int();
	test_metodi_int();
	test_add_int();
	test_remove_int();

	cbuffer<int> db(10, 3);
	test_const_int(db);
  
  test_iterator_int();
  test_const_iterator_int();
  test_ctor_iterators();
  
  test_char_type();
  test_point_type();

	return 0;
}
















