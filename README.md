# Circular Buffer

## Description
The template class represents a circular array cbuffer. The size of the array can be chosen at construction time, and the data will be overwritten once the space is exhausted, starting from the first cell of the array. The circular characteristic is only logical because it is physically implemented with a static array. The class handles the allocation and deallocation of memory without causing memory leaks.

The class implements iterator and const_iterator with all available operators: pre and post-increment, and comparison operators.

## Data types
```c++
typedef unsigned int size_type;
typedef int index_type;
typedef T value_type;
```

## Methods
#### Default constructor
Default constructor to instantiate an empty cbuffer.
```c++
cbuffer() : _size(0), _filledSize(0), _buffer(nullptr), _head(0), _tail(-1)
```

#### Size constructor
Secondary constructor to instantiate a cbuffer with a fixed size.
```c++
explicit cbuffer(size_type size) : _size(0), _filledSize(0), _buffer(nullptr), _head(0), _tail(-1)
```

#### Size & init constructor
Secondary constructor to instantiate a cbuffer with a fixed size and initialize the array cells with a passed value.
```c++
cbuffer(size_type size, const value_type &value) : _size(0), _filledSize(0), _buffer(nullptr), _head(0), _tail(-1)
```

#### Copy constructor
Copy constructor between two a cbuffer.
```c++
cbuffer(const cbuffer &other) : _size(0), _filledSize(0), _buffer(nullptr), _head(0), _tail(-1)
```

#### Iterators constructor
Constructor to generate a cbuffer with size dimension filled with a sequence of data identified by a pair of generic iterators passed by argument.
```c++
template <typename Iter>
cbuffer(size_type size, Iter b, Iter e) : _size(0), _filledSize(0), _buffer(nullptr), _head(0), _tail(-1)
```

#### Assignment operator
Assignment operator to copy the contents of a cbuffer other into the cbuffer this.
```c++
cbuffer<T>& operator=(const cbuffer<T> &other)
```

#### Swap
Method to exchange the contents of two cbuffers.
```c++
void swap(cbuffer &other)
```

#### Clear
Method that cleans the contents of a cbuffer.
```c++
void clear()
```

#### Operator [] get/set
Method to read and/or write the i-th cell of the circular array between head and tail.
```c++
value_type& operator[](index_type index)
```

#### Operator [] get
Method to read the i-th cell of the circular array.
```c++
const value_type& operator[](index_type index) const
```

#### Size
Returns the total size of the circular array.
```c++
size_type size() const
```

#### Filled size
Returns the currently occupied size of the circular array.
```c++
size_type filledSize() const
```

#### Head intex
Returns the start index of a cbuffer.
```c++
index_type getHead() const
```

#### Tail index
Returns the end index of a cbuffer.
```c++
index_type getTail() const
```

#### Tail addition
Inserts a new item in the queue than those in the cbuffer, if the cbuffer is full it overwrites the oldest data.
```c++
void addTail(const value_type &value)
```

#### Head removal
Removes the element located at the head of the cbuffer.
```c++
void removeHead()
```

#### Stream operator
Redefinition of stream operator to write a cbuffer to an output stream, the circular logical content is printed and physically arranged in memory.
```c++
friend std::ostream &operator<<(std::ostream &os, const cbuffer &cb)
```
